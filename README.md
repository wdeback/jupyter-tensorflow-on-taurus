# UPDATE (September 2019)

Now, ZIH provides a JupyterHub interface to spawn Jupyter notebooks. This renders this description **outdated**.

Documentation on using JupyterHub: https://doc.zih.tu-dresden.de/hpc-wiki/bin/view/Compendium/JupyterHub

Access Taurus via JupyterHub: https://taurus.hrsk.tu-dresden.de


Introduction (OUTDATED)
------------

This walkthrough shows you how to:
- set up a jupyter notebook server on [Taurus](https://doc.zih.tu-dresden.de/hpc-wiki/bin/view/Compendium/SystemTaurus)
- access it remotely via ssh tunnel
- use it to run keras/tensorflow models


1. Install python & jupyter via conda  
-------------------------------------

- Log into taurus:

```
ssh -Y <username>@tauruslogin5.hrsk.tu-dresden.de
```

(To log in from home, set up a VPN via [Cisco Anyconnect](
https://tu-dresden.de/zih/dienste/service-katalog/arbeitsumgebung/zugang_datennetz/vpn/ssl_vpn/index
))

- Download and install conda to get python, numpy, scipy, pandas and jupyter etc. from Anaconda: https://www.anaconda.com/download

```bash
wget https://repo.continuum.io/archive/Anaconda3-4.4.0-Linux-x86_64.sh
```

- Install conda:

```bash
./Anaconda3-4.4.0-Linux-x86_64.sh
```

- Create an environment, here called 'nb', and activate it

```bash
conda create --name nb
source activate nb
```

- Install packages if they were not installed. 
Note: you do not need to install the keras/tensorflow/CUDA/CuDNN toolchain because we'll be using the one supplied globally on Taurus.

```bash
conda install jupyter
```

- **Alternatively** install jupyter via pip

I use conda here, but you can also install it via pip:

```bash
module load modenv/eb
module load tensorflow #loads Python as dependency
pip install --user jupyter
```


2. Setting up public Jupyter remote server
-------------------------------------
Source: https://jupyter-notebook.readthedocs.io/en/latest/public_server.html#running-a-public-notebook-server


- Create a config file for the notebook using the following command line:

```bash
jupyter notebook --generate-config
```

- Create a password (and copy it to clipboard)

```bash
jupyter notebook password
```

- Generate a self-signed certificate with openSSL

```bash
openssl req -x509 -nodes -days 365 -newkey rsa:1024 -keyout mykey.key -out mycert.pem
```

- In `~/.jupyter/jupyter_notebook_config.py`, find, uncomment and edit these files

```python
# Set options for certfile, ip, password, and toggle off
# browser auto-opening
c.NotebookApp.certfile = u'/home/<username>/mycert.pem'
c.NotebookApp.keyfile = u'/home/<username>/mykey.key'
# Set ip to '*' to bind on all interfaces (ips) for the public server
c.NotebookApp.ip = '*'
c.NotebookApp.password = u'sha1:bcd259ccf...<your hashed password here>'
c.NotebookApp.open_browser = False

# It is a good idea to set a known, fixed port for server access
c.NotebookApp.port = 9999
```

Note that this uses a self-signed certificate. It's safer to create a certificate as described here:
 https://jupyter-notebook.readthedocs.io/en/latest/public_server.html#using-let-s-encrypt

3. Running Jupyter server on Taurus
--------------------------------------------

- Create a SLURM job script to start `jupyter notebook` on a GPU partition of Taurus

```bash
#!/bin/bash -l
#SBATCH --gres=gpu:1 # request GPU
#SBATCH --partition=gpu1,gpu2 # use GPU partition
#SBATCH --output=output.txt
## ... other settings

module purge
module load modenv/eb
module load Python
module load Keras # load keras first to avoid issue with cudnn 6.0
module load tensorflow

srun jupyter notebook
```

Save this script as `start_notebook.slurm` (my full script is on bottom of page).

- Start the notebook as a SLURM job:

```bash
sbatch start_notebook.slurm
```

- Wait for the job to start and get the name of the compute node on which the job is running (here: taurusi2098)

```bash
$squeue -u <username>
JOBID   PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
10696924      gpu2 start_no   <username>  R      0:05      1 taurusi2098
```

- Make sure the notebook is running on port 9999 by checking the output file. Look for a line like the one below:

```bash
$ tail -n 20 output.txt
[I 09:38:57.180 NotebookApp] The Jupyter Notebook is running at: https://[all ip addresses on your system]:9999/
```

4. Connect to remote notebook via ssh
--------------------------------------------------

- Open a terminal on your local machine and create a SSH tunnel directly to the compute node to connect to the notebook:

```bash
node=taurusi2098
localport=8888
ssh -fNL ${localport}:${node}:9999 <username>@tauruslogin4.hrsk.tu-dresden.de
pgrep -f "ssh -fNL ${localport}"
```

The first two lines specify the name of the remote compute node (`node`) and the local port to listen to (`localport`). 
The third line creates the ssh tunnel (`-L`) and send the tunnel process to the background (`-fN`). The downside of this is that you’ll have to look up the process and kill it manually when you want to close the tunnel.
The last line looks up and print the PID of the ssh tunnel such that you can kill it later with `pkill`.


- Open a web browser and point the browser to 

```bash
https://localhost:8888
```

Note:
  - Notice the http**s**. The browser may complain about the self-signed certificate, but you can ignore this warning. If this concerns you, please go back to the remote host and generate a non-self-signed certificate, see: https://jupyter-notebook.readthedocs.io/en/latest/public_server.html#using-let-s-encrypt.
  - Replace 8888 with the value for `localport` you have specified before.
  - Note that it may take a while before you get a connection. Be patient or reload the page a few times.


5. Test the notebook
--------------------

You should now have a running Jupuyter notebook! Yuhuu! But before getting too enthusiastic, let's test it.

- Test python version

```python
import platform
print(platform.python_version())
```

This should output "3.5.2" or newer


- Check python path

```python
import os
os.environ['PYTHONPATH'].split(os.pathsep)
```

This should output something like this (depending on the modules that you loaded):

```python
['/sw/taurus/eb/matplotlib/1.5.3-intel-2016.03-GCC-5.3-Python-3.5.2/lib/python3.5/site-packages',
 '/sw/taurus/eb/scikit-image/0.12.3-intel-2016.03-GCC-5.3-Python-3.5.2/lib/python3.5/site-packages',
 '/sw/taurus/eb/Pillow/3.4.2-intel-2016.03-GCC-5.3-Python-3.5.2/lib/python3.5/site-packages',
 '/sw/taurus/eb/Keras/2.0.2-intel-2016.03-GCC-5.3-Python-3.5.2/lib/python3.5/site-packages',
 '/sw/taurus/eb/PyYAML/3.12-intel-2016.03-GCC-5.3-Python-3.5.2/lib/python3.5/site-packages',
 '/sw/taurus/eb/h5py/2.6.0-intel-2016.03-GCC-5.3-Python-3.5.2-HDF5-1.8.17-serial/lib/python3.5/site-packages',
 '/sw/taurus/eb/pkgconfig/1.1.0-intel-2016.03-GCC-5.3-Python-3.5.2/lib/python3.5/site-packages',
 '/sw/taurus/eb/Theano/0.9.0-intel-2016.03-GCC-5.3-Python-3.5.2/lib/python3.5/site-packages',
 '/sw/taurus/eb/libgpuarray/0.6.2-intel-2016.03-GCC-5.3/lib/python3.5/site-packages',
 '/sw/taurus/eb/tensorflow/1.2.1-Python-3.5.2/lib/python3.5/site-packages']
```

It looks like you're all set to go. And this is true, except when you want to use tensorflow.

- Now, let's check whether we can import tensorflow:

```python
import tensorflow
```

Unfortunately, this raises an error:

```python
ImportErrorTraceback (most recent call last)
<ipython-input-3-a649b509054f> in <module>()
----> 1 import tensorflow

/sw/taurus/eb/tensorflow/1.2.1-Python-3.5.2/lib/python3.5/site-packages/tensorflow/__init__.py in <module>()
     22 
     23 # pylint: disable=wildcard-import
---> 24 from tensorflow.python import *
     25 # pylint: enable=wildcard-import
     26 

/sw/taurus/eb/tensorflow/1.2.1-Python-3.5.2/lib/python3.5/site-packages/tensorflow/python/__init__.py in <module>()
     45 # pylint: disable=wildcard-import,g-bad-import-order,g-import-not-at-top
     46 
---> 47 import numpy as np
     48 
     49 from tensorflow.python import pywrap_tensorflow

....

ImportError: dynamic module does not define init function (initmultiarray)
```

Note: your error may look different (here, it is caused by a conflict between python2 and python3 packages), but you **will** get an error. 


6. Creating and editing a new Jupyter kernel
-------------------------------------------------

The error above is caused by the fact that the python kernel that jupyter uses is not the one you that have loaded with `module load Python` in the SLURM script.

- To solve this issue, we will create a new kernel manually.

```bash
# 1. jump into the environment where you installed jupyter
## NOTE: this should be done BEFORE loading the modules
source activate nb

# 2. load the correct Python version from the modules (as in SLURM script)
## This overrides the python version in the environment
module load modenv/eb
module load Python

# 3. install a new Jupyter kernel
python -m ipykernel install --user
```

- Test it again.

Rerun the steps above:
  - run `sbatch start_notebook.slurm`
  - create ssh tunnel and open `https://localhost:8889` or other port
  - select python kernel and run
  
```python
import tensorflow
```

**Update**: On Taurus partition 'gpu2', importing tensorflow now works without the hack described below.

You will be greeted with this error (abbreviated here):

```python
---------------------------------------------------------------------------
ImportError                               Traceback (most recent call last)
/sw/taurus/eb/tensorflow/1.2.1-Python-3.5.2/lib/python3.5/site-packages/tensorflow/python/pywrap_tensorflow.py in <module>()
     40     sys.setdlopenflags(_default_dlopen_flags | ctypes.RTLD_GLOBAL)
---> 41   from tensorflow.python.pywrap_tensorflow_internal import *
     42   from tensorflow.python.pywrap_tensorflow_internal import __version__

/sw/taurus/eb/tensorflow/1.2.1-Python-3.5.2/lib/python3.5/site-packages/tensorflow/python/pywrap_tensorflow_internal.py in <module>()
     27             return _mod
---> 28     _pywrap_tensorflow_internal = swig_import_helper()
     29     del swig_import_helper

/sw/taurus/eb/tensorflow/1.2.1-Python-3.5.2/lib/python3.5/site-packages/tensorflow/python/pywrap_tensorflow_internal.py in swig_import_helper()
     23             try:
---> 24                 _mod = imp.load_module('_pywrap_tensorflow_internal', fp, pathname, description)
     25             finally:

/sw/taurus/eb/Python/3.5.2-intel-2016.03-GCC-5.3/lib/python3.5/imp.py in load_module(name, file, filename, details)
    241         else:
--> 242             return load_dynamic(name, filename, file)
    243     elif type_ == PKG_DIRECTORY:

/sw/taurus/eb/Python/3.5.2-intel-2016.03-GCC-5.3/lib/python3.5/imp.py in load_dynamic(name, path, file)
    341             name=name, loader=loader, origin=path)
--> 342         return _load(spec)
    343 

ImportError: /lib64/libc.so.6: version `GLIBC_2.16' not found (required by /sw/taurus/eb/tensorflow/1.2.1-Python-3.5.2/lib/python3.5/site-packages/tensorflow/python/_pywrap_tensorflow_internal.so)

Failed to load the native TensorFlow runtime.

See https://www.tensorflow.org/install/install_sources#common_installation_problems

for some common reasons and solutions.  Include the entire stack trace
above this error message when asking for help.
```

What is going on here??
-----------------------

The telltale line in the error message is this:

```python
ImportError: /lib64/libc.so.6: version 'GLIBC_2.16' not found (required by /sw/taurus/eb/tensorflow/1.2.1-Python-3.5.2/lib/python3.5/site-packages/tensorflow/python/_pywrap_tensorflow_internal.so)
```

The problem is described here: https://doc.zih.tu-dresden.de/hpc-wiki/bin/view/Compendium/DeepLearning

> TensorFlow is available in our EasyBuild module environment, with a little caveat: since the binary distribution packages are built with GLIBC 2.16 and the operating system on Taurus only includes GLIBC 2.12, it is not possible to use this library out-of-the-box. There is, however, a workaround which is described in the following.
> The module "tensorflow" includes a wrapper script for the Python interpreter called "python-glibc2.17" which can be used as a shebang for your scripts and then will launch Python using a newer GLIBC version, thus making it possible to utilize TensorFlow despite the old GLIBC version from the system. Hopefully, this won't be necessary anymore later this year when we upgrade our cluster to the latest bullx Linux Release SCS5 (based on RHEL7) which finally sports a more up-to-date GLIBC version.

- This can be solved by editing the kernel we have just created:

Open the file `/home/<username>/.local/share/jupyter/kernels/python3/kernel.json` and replace the 

```json 
"/sw/taurus/eb/Python/3.5.2-intel-2016.03-GCC-5.3/bin/python",
```

with 

```json
  "/sw/taurus/eb/tensorflow/1.2.1-Python-3.5.2/bin/python-glibc2.17",
```

such that it reads:

```json
{
 "language": "python",
 "argv": [
  "/sw/taurus/eb/tensorflow/1.2.1-Python-3.5.2/bin/python-glibc2.17",
  "-m",
  "ipykernel_launcher",
  "-f",
  "{connection_file}"
 ],
 "display_name": "Python 3"
}
```


7. Running a Jupyter notebook with Keras/Tensorflow on Taurus 
-------------------------------

Finally, we have set everything up correctly. 

Here are the steps that we need to go through *each time we want to start and run a notebook session*:

- Start the jupyter notebook server on the remote machine: 

```bash 
# log onto taurus
ssh -Y <username>@tauruslogin5.hrsk.tu-dresden.de

# go to folder containing start_notebook.slurm
cd path/to/notebook/slurm/script

# submit job
sbatch start_notebook.slurm

# wait for job to start...

# check the node on which the job is running
# note the name under NODELIST
squeue -u <username>
```

- On your local machine, create a ssh tunnel 

```bash
node=taurusi2098
localport=8888
ssh -fNL ${localport}:${node}:9999 <username>@tauruslogin4.hrsk.tu-dresden.de
pgrep -f "ssh -fNL ${port}"
```

Note: if `ssh` gives you this error:

```bash
bind: Address already in use
channel_setup_fwd_listener_tcpip: cannot listen to port: 8888
```

Please select a different port, e.g. `localport=8889` and recreate the `ssh` tunnel.

Alternatively, first kill the ssh tunnel using the PID that is printed by the command `pgrep -f "ssh -fNL ${port}"` like `pkill 2345`.


- Open a web browser and point the browser to 

```bash
https://localhost:8888
```

It may take a while befor ethe connection is established. Be patient or reload the page a few times.

- Test tensorflow

```python
import tensorflow
```

This should give you the friendlier response: 

```python
Using TensorFlow backend.
```

Congrats. You now have a notebook set up that is able to make proper use of the Taurus' GPUs.

- Training a convnet.

Let's go ahead and run a deep learning test using Keras to train a convnet. 
Here I'll use a simple convnet on the MNIST data set, copy/pasting the code from here in the notebook: https://github.com/fchollet/keras/blob/master/examples/mnist_cnn.py

```python
x_train shape: (60000, 28, 28, 1)
60000 train samples
10000 test samples
Train on 60000 samples, validate on 10000 samples
Epoch 1/12
60000/60000 [==============================] - 12s - loss: 0.3254 - acc: 0.9004 - val_loss: 0.0815 - val_acc: 0.9739
Epoch 2/12
60000/60000 [==============================] - 10s - loss: 0.1155 - acc: 0.9664 - val_loss: 0.0555 - val_acc: 0.9829
Epoch 3/12
60000/60000 [==============================] - 10s - loss: 0.0863 - acc: 0.9744 - val_loss: 0.0424 - val_acc: 0.9865
Epoch 4/12
60000/60000 [==============================] - 10s - loss: 0.0744 - acc: 0.9781 - val_loss: 0.0393 - val_acc: 0.9870
Epoch 5/12
60000/60000 [==============================] - 10s - loss: 0.0630 - acc: 0.9816 - val_loss: 0.0361 - val_acc: 0.9875
Epoch 6/12
60000/60000 [==============================] - 10s - loss: 0.0548 - acc: 0.9835 - val_loss: 0.0327 - val_acc: 0.9881
Epoch 7/12
60000/60000 [==============================] - 10s - loss: 0.0507 - acc: 0.9851 - val_loss: 0.0306 - val_acc: 0.9883
Epoch 8/12
60000/60000 [==============================] - 10s - loss: 0.0480 - acc: 0.9858 - val_loss: 0.0290 - val_acc: 0.9894
Epoch 9/12
60000/60000 [==============================] - 10s - loss: 0.0441 - acc: 0.9869 - val_loss: 0.0276 - val_acc: 0.9902
Epoch 10/12
60000/60000 [==============================] - 10s - loss: 0.0415 - acc: 0.9874 - val_loss: 0.0276 - val_acc: 0.9906
Epoch 11/12
60000/60000 [==============================] - 10s - loss: 0.0394 - acc: 0.9882 - val_loss: 0.0288 - val_acc: 0.9905
Epoch 12/12
60000/60000 [==============================] - 10s - loss: 0.0364 - acc: 0.9890 - val_loss: 0.0271 - val_acc: 0.9903
Test loss: 0.0270963933624
Test accuracy: 0.9903
```

On the Tesla K40/K80 GPUs that Taurus provides, this results in a speed of ~10s per epoch. Not bad at all! 


8. My SLURM script
-----------------

For completeness, here is my SLURM script `start_notebook.slurm`

```bash
#!/bin/bash -l

#SBATCH --gres=gpu:1 # change this to ask for multiple GPUs
#SBATCH --partition=gpu1,gpu2 # the Tesla K80 with 11Gb mem are in partition 'gpu2' 
#SBATCH --output=output.txt

#SBATCH --ntasks=1
#SBATCH --time=5:00:00 # the jupyter notebook server and any job it is performing will be killed after this time, use checkpointing!
#SBATCH --mem=10000 # this is the CPU mem

source activate nb # activate the environment in which you installed jupyter with the edited kernel. Needs to be done BEFORE loading the modules.

module --force purge
module load modenv/eb
module load Python
## modules for deep learning
module load Keras # load keras first to avoid issue with cudnn 6.0
module load tensorflow
## module for plotting
module load matplotlib/1.5.3-intel-2016.03-GCC-5.3-Python-3.5.2
## modules for image processing
module load Pillow
module load scikit-image/0.12.3-intel-2016.03-GCC-5.3-Python-3.5.2

srun jupyter notebook
```










